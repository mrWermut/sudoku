import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { GameField } from './models/gamefield';
import { Cell } from './models/cell';

@Injectable({
  providedIn: 'root'
})
export class WebApiService {

  constructor(private _httpClient: HttpClient ) { }

  MakeProblems(difficulty: number, count: number): Observable<string> {
     return this._httpClient.get<string>( '/api/Game/NewGame?difficulty=' + difficulty + '&count=' + count );
  }

  GetProblem(problemId: number): Observable<GameField> {
    return this._httpClient.get<GameField>('api/Game/GetProblem?problemId=' + problemId );
  }

  ValidateSolution(cellsToValidate: Cell[], problemId: number): Observable<string> {
    return this._httpClient.post<string>('/api/Game/ValidateSolution?problemId=' + problemId , cellsToValidate );
  }

}
