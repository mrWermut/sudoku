import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { GameBoardComponent } from './game-board.component';
import { FormsModule } from '@angular/forms';
import { GameBoardService } from './game-board.service';
import { TooltipModule } from 'ngx-bootstrap/tooltip';
import { ButtonsModule } from 'ngx-bootstrap/buttons';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    ButtonsModule,
    TooltipModule
  ],
  declarations: [GameBoardComponent],
  exports: [GameBoardComponent],
  providers: [GameBoardService]

})
export class GameBoardModule { }
