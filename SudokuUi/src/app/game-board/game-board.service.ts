import { Injectable } from '@angular/core';
import { Observable, BehaviorSubject, of } from 'rxjs';
import { map,  tap, catchError } from 'rxjs/operators'; // TODO: error handling
import { flatten} from 'lodash';
import { cloneDeep } from 'lodash';
import {Cell} from '../models/cell';

import {WebApiService} from '../web-api.service';
import {MessagingService} from '../messaging.service';

@Injectable({
  providedIn: 'root'
})
export class GameBoardService {

  // private _gameBoard = new BehaviorSubject<Cell[][]>(null);
  // gameBoard$ = this._gameBoard.asObservable();
  currentBoard: Cell[][];
  private _originalCells: Cell[];

  get originalCells(): Cell[] {
    return this._originalCells;
  }

  constructor(private _webApi: WebApiService , private _messenger: MessagingService ) {  }

  getProblem(problemId: number) {
    return this._webApi.GetProblem(problemId).pipe(
      // tap(field => console.log('GameBoardService current problem:', field.grid)),
      map(field => field.grid),
      tap( cells => {
        cells.forEach(r => r.forEach( c => {
          c.focused = false;
          if (c.isHint === true) { c.visible = true; } else { c.visible = false; }
          } )); // totally legal in js
      //  this._gameBoard.next(cells);
        this.currentBoard = cells;
        const flat = flatten(cells);
        this._originalCells =  cloneDeep(flat);
      })
    ).subscribe();

  }

  generateProblems(count: number) {
    this._webApi.MakeProblems(0, count).subscribe(s => console.log('GameBoardService current problem: Problems created'));
  }

  validateSolution(cellsToValidate: Cell[], problemId: number) {
 //   cellsToValidate [0] = {axisX: 0, axisY: 0, value: 2, isHint: false }; // mock

    this._webApi.ValidateSolution(cellsToValidate, problemId).subscribe(status => { this._messenger.notify(status); });
  }

}
