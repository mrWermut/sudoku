import { GameBoardModule } from './game-board.module';

describe('GameBoardModule', () => {
  let gameBoardModule: GameBoardModule;

  beforeEach(() => {
    gameBoardModule = new GameBoardModule();
  });

  it('should create an instance', () => {
    expect(gameBoardModule).toBeTruthy();
  });
});
