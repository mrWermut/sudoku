import { Component, OnInit, OnDestroy } from '@angular/core';
import {WebApiService} from '../web-api.service';
import {Cell} from '../models/cell';
import {GameBoardService} from './game-board.service';

const PROBLEMS_COUNT = 5 ;

@Component({
  selector: 'app-game-board',
  templateUrl: './game-board.component.html',
  styleUrls: ['./game-board.component.css']
})


export class GameBoardComponent implements OnInit {

  private _prevFocusedCell: Cell;
  private _currentFocusedCell: Cell;
  private _problemId = 0;
  gameSet = false;
  toggledMenuNumber: number = 0;

  private _pickedCells: Cell[] = [];
  numbersMenu = [0, 1, 2, 3, 4, 5, 6, 7, 8, 9];
  gameDifficulty = [{key: 'Beginner', value: 0}, {key: 'Intermediate', value: 1}, {key: 'Advanced', value: 2}]; // not implemented yet

  // get gameBoard$() { return this._gameBoardService.gameBoard$; }
  get currentBoard() { return this._gameBoardService.currentBoard; }
  get originalCells() { return this._gameBoardService.originalCells; }
  get problemIds() { return Array.from(Array(PROBLEMS_COUNT).keys()); }

  constructor(private _webApi: WebApiService, private _gameBoardService: GameBoardService ) {  }

  ngOnInit() {  }


   generateProblemsClick() {
     this._gameBoardService.generateProblems(PROBLEMS_COUNT);
     this._gameBoardService.getProblem(0);
     this.gameSet = true;
   }

   getProblemClick(problemId) {

     this.flush();
     this.gameSet = true;
     this._gameBoardService.getProblem(problemId);
     this._problemId = problemId;
   }

   validateSolutionClick() {
     if (this._pickedCells && this._pickedCells.length > 0) {
       this._gameBoardService.validateSolution(this._pickedCells, this._problemId);
     }
   }

   focusCell(cell: Cell, event: any) {
     if (cell.isHint === true){
       return;
     }

     cell.focused = true;
     this._currentFocusedCell = cell;

     if (event.ctrlKey ) {
         this.getHint();
         return;
     }

     if (this._prevFocusedCell === undefined) {
       this._prevFocusedCell = cell;
     }


     if (this._prevFocusedCell !== this._currentFocusedCell) {
       this._prevFocusedCell.focused = false;
       this._prevFocusedCell = cell;
     }

   }

   getHint() {
    this._currentFocusedCell.value = this.getOriginalCellValue(this._currentFocusedCell);
    this._currentFocusedCell.isHint = true;
    this._currentFocusedCell.visible = true;
     this._pickedCells.push({
       axisX: this._currentFocusedCell.axisX,
       axisY: this._currentFocusedCell.axisY,
       value: this._currentFocusedCell.value,
       isHint: false, // for the correct server-side evaluations
       focused: null,
       visible: null});
   }


   makeProposal() {
     if (this._currentFocusedCell.isHint !== true) {
       this._currentFocusedCell.value = this.toggledMenuNumber;
       this._prevFocusedCell.visible = true;
       this._pickedCells.push(this._currentFocusedCell);
     }

     if (this.toggledMenuNumber > 0) {
       if (this._pickedCells.indexOf(this._currentFocusedCell) < 0) {
          this._pickedCells.push(this._currentFocusedCell);
       }
     } else {
       // REWIND
         this._pickedCells = this._pickedCells.filter(cell => cell !== this._currentFocusedCell);
         this._currentFocusedCell.value = this.getOriginalCellValue(this._currentFocusedCell);
         this._currentFocusedCell.visible = false;
     }
   }

  pickNumber(num: number) {
    this.toggledMenuNumber = num;
  }
  getMenuNumberClass(num) {
    if (num === this.toggledMenuNumber) { return 'num-box noselect menu-number-focused'; } else {return 'num-box noselect'; }
  }

  getBoxClass(cell: Cell): string {
    if (cell.isHint === true) {
      return 'noselect box box-hint';
    }
    if (cell.focused === true) {
      return 'noselect box box-focused';
    }
      return 'noselect box';
  }

  private getOriginalCellValue(cell: Cell ): number {
    const originalCells = this._gameBoardService.originalCells;
    const  orinalCell = originalCells.find( (c) => c.axisY === this._currentFocusedCell.axisY && c.axisX === this._currentFocusedCell.axisX );
    return orinalCell.value;
  }

   private flush() {
    this._currentFocusedCell = undefined;
    this._pickedCells = [];
    this._prevFocusedCell = undefined;

   }


}
