import { Injectable } from '@angular/core';
import { ToastrService } from 'ngx-toastr';
import { IndividualConfig } from 'ngx-toastr/toastr/toastr-config';
import { HttpErrorResponse } from '@angular/common/http'; // TODO:  http error handling

const defaultConfig = {
  tapToDismiss: true,
  fadeIn: 300,
  fadeOut: 1000,
  positionClass: 'toast-top-left',
  enableHtml: true
}


@Injectable({
  providedIn: 'root'
})
export class MessagingService {

  constructor(private _toastrService: ToastrService) { }


  notify(message: any) {
       console.log(message.status);
       switch (message.status) {
       case 'GoodToGo': { this.info("You're on the right way!", 'Good to go', defaultConfig); break; }
       case 'Solved': { this.success('Problem solved!', 'Congratulations', defaultConfig);  break; }
       case 'WrongProposal': { this.error('There are errors in Your solution!', 'Wrong!', defaultConfig);  break; }
     }
  }

  private error(text?: string, title?: string, config?: Partial<IndividualConfig>) {
    this._toastrService.error(text, title, config);
  }
  private warning(text?: string, title?: string, config?: Partial<IndividualConfig>) {
    this._toastrService.warning(text, title, config);
  }

  private info(text?: string, title?: string, config?: Partial<IndividualConfig>) {
    this._toastrService.info(text, title, config);
  }

  private success(text?: string, title?: string, config?: Partial<IndividualConfig>) {
    this._toastrService.success(text, title, config);
  }
}
