import {Cell} from '../models/cell';

export interface GameField {
   grid: Cell[][];
}
