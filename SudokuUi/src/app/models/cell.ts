export interface Cell {
   axisX: number;
   axisY: number;
   isHint: boolean;
   value: number;
   focused: boolean;
   visible: boolean;
}
