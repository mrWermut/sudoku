﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Sudoku.Models
{
    public class ValidateSolutionResponce
    {
        public string Status{ get; set; }
        public ValidateSolutionResponce(SolutionStatus status) {
             Status = status.ToString();
        }
    }
}
