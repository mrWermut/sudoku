﻿namespace Sudoku.Models
{
    public class GameField
    {
        public GameField(Cell[][] grid) {
            Grid = grid;
        }

        public Cell[][] Grid { get; set; }
    }
}