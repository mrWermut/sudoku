﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Sudoku.Models
{
    public class Cell
    {
        public int Value { get; set; }
        public bool IsHint { get; set; }
        public int AxisX { get; set; }
        public int AxisY { get; set; }
        public Cell(int value, bool  isHint) {
            Value = value;
            IsHint = isHint;

           
        }
    }
}
