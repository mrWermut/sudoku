﻿using System;
using Sudoku.Models;
using System.Linq;
using System.Collections;

namespace Sudoku.Generators
{
    public class GameFieldGenerator
    {
      
        public GameField GenetateField(Difficulty difficulty) {
 
            var grid =  new Cell [9][];
            for (int i = 0; i < 9; i++)
            {
                var cellRow =  new Cell[9];

                for (int j = 0; j < 9; j++)
                {
                    var value = (i * 3 + i / 3 + j) % 9 + 1;
                    cellRow[j] = new Cell(value: value, isHint: false );
                    //TODO: не забыть убрать этот кусок потом, когда заработает перемешиванние 
                    cellRow[j].AxisX = j;
                    cellRow[j].AxisY = i; 
                
                }
                grid[i] = cellRow;

            }
  
            var topN = 0;
            switch (difficulty)
            {
                case Difficulty.Beginner:
                    topN = 35;
                    break;
                case Difficulty.Intermidiate:
                    topN = 30;
                    break;
                case Difficulty.Advanced:
                    topN = 25;
                    break;
            }

            Random rnd = new Random();

            // flatten
            var topCells = grid.SelectMany(a =>a).OrderBy(x => rnd.Next()).Take(topN).ToArray();
            // make visible update 
            topCells.ToList().ForEach(cell => cell.IsHint = true);

            var field = new GameField(grid);
            return field;

        }

        //TODO: доделать взрослый генератор 
        private Cell[][] Transpond(Cell[][] grid) {
            throw new NotImplementedException(); 


        }
      
        private Cell[][] SwapRows(Cell[][] grid)
        {
            throw new NotImplementedException();

        }

        private   Cell[][] SwapColumns(Cell[,] grid)
        {
            throw new NotImplementedException();

        }
    }
}
