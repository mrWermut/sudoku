﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
 
using Sudoku.Generators;
using Sudoku.Models;

using Swashbuckle.AspNetCore.Annotations;

using System.IO;
using System.Text;

namespace Sudoku.Web.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class GameController : Controller
    {
 

        [HttpGet("NewGame")]
        [SwaggerOperation("NewGameReqest")]
        public void  MakeProblems(Difficulty difficulty, int count)
        {
            var generator = new GameFieldGenerator();
            var boards = new GameField[count];

            for (int i = 0; i < count; i++) {
                boards[i] = generator.GenetateField(difficulty);

                var path = $"../dummy_sudoku{i}.json";
                var output = String.Empty;
                output = Newtonsoft.Json.JsonConvert.SerializeObject(boards[i]);
                
                using (var fs = new FileStream(path, FileMode.OpenOrCreate, FileAccess.ReadWrite, FileShare.Read))
                {
                    using (var streamWriter = new StreamWriter(fs, Encoding.UTF8))
                    {
                        streamWriter.Write(output);
                    }
                }          
            }
       
        }

        [HttpGet("GetProblem")]
        [SwaggerOperation("GetProblemRequest")]
        public GameField GetProblem( int problemId)
        {
            var path = $"../dummy_sudoku{problemId}.json";
            var input = String.Empty;
            if (System.IO.File.Exists(path))
            {
                using (var fs = new FileStream(path, FileMode.Open, FileAccess.Read, FileShare.ReadWrite))
                {
                    using (var streamReader = new StreamReader(fs, Encoding.UTF8))
                    {
                        input = streamReader.ReadToEnd();
                    }
                }

            }
            
            return Newtonsoft.Json.JsonConvert.DeserializeObject<GameField>(input);
        }


        [HttpPost("ValidateSolution")]
        [SwaggerOperation("ValidateSolutionRequest")]
        public ValidateSolutionResponce ValidateSolution([FromBody] Cell[] cellsToValidate, int problemId)
        {
            cellsToValidate = cellsToValidate.Distinct().ToArray(); // dooublecheck!

            var path = $"../dummy_sudoku{problemId}.json";
            var input = String.Empty;
            if (System.IO.File.Exists(path))
            {
                using (FileStream fs = new FileStream(path, FileMode.Open, FileAccess.Read, FileShare.ReadWrite))
                {
                    using (StreamReader streamReader = new StreamReader(fs, Encoding.UTF8))
                    {
                        input = streamReader.ReadToEnd();
                    }
                }

            }
            
            var currentProblem =  Newtonsoft.Json.JsonConvert.DeserializeObject<GameField>(input);
            var currentCells = currentProblem.Grid.SelectMany(a => a).ToArray();

            var notHintsCount = currentCells.Where(c => c.IsHint == false).ToArray().Length; 
            var joined = cellsToValidate.Join(currentCells,
                                            inner => new { inner.AxisX,  inner.AxisY, inner.Value },
                                            outer => new { outer.AxisX, outer.AxisY, outer.Value },
                                            (inner,outer) => { return inner;  }
                 ).ToArray();

            if (joined.Length == notHintsCount)
                return new ValidateSolutionResponce(SolutionStatus.Solved);

            if (joined.Length == cellsToValidate.Length)
                return new ValidateSolutionResponce(SolutionStatus.GoodToGo);
            else
                return new ValidateSolutionResponce(SolutionStatus.WrongProposal);
        }
 

    }
}